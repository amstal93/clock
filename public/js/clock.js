setInterval(() => {
    const time = Date();
    const timearray = time.split(' ');
    const weekday = timearray[0];
    const month = timearray[1];
    const day = timearray[2];
    const year = timearray[3];
    const timetime = timearray[4];
    const timetimearray = timetime.split(':');
    const hour = timetimearray[0];
    const minute = timetimearray[1];
    const second = timetimearray[2];
    const timezone = timearray[5];
    const timezonename = timearray[6].slice(1,-1);
    document.getElementById("weekday").innerHTML = weekday;
    document.getElementById("hours").innerHTML = hour;
    document.getElementById("minutes").innerHTML = minute;
    document.getElementById("seconds").innerHTML = second;
    document.getElementById("day").innerHTML = day;
    document.getElementById("month").innerHTML = month;
    document.getElementById("year").innerHTML = year;
}, 10);

function displaySeconds() {
    const secondElements = document.getElementsByClassName("seconds");
    let style = '';
    if (document.getElementById("toggleSeconds").checked) {
        style = '';
    } else {
        style = 'none';
    }

    for (const element of secondElements) {
        element.style.display = style
    }
}